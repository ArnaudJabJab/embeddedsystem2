# EmbeddedSystem2

Project Developed by Arnaud Jabiol & Hugo Dalger for BJTU Embedded System Application Development module.

Build of a new Raspberry4 Kernel from source code.

Use of cross-compiling to build it.



Build and cross-compiling
-----
We followed the tutorial of the [official website](https://www.raspberrypi.org/documentation/linux/kernel/building.md) in order to clone and create the cross-compilation using ubuntu.

Because we do not have an SD Card there, we stopped at the step _Install directly onto the SD card"_  of the official tutorial.
We will able to test our image directly by moving our new kernel build into the `/boot` directory of a raspberry.

To implement the cross-compiling, we followed a tutorial present [here](https://fdmobileinventions.blogspot.com/2019/01/cross-compiling-linux-kernel-for.html)


To resume the tutorial, the kernel installation consists of three parts:

* kernel.img (Pi 0/1), kernel7.img (Pi 2/3), kernel7l (Pi 4): The kernel image itself
* The modules folder: Contains all preinstalled kernel modules
* Overlays: Configurations for the RPi board and peripherals

Installation of the new Kernel
-----
Once we packed all of this, we have 2 packages named modules.tgz  and boot.tgz (image + overlays).
With these 2 packages, we only need to implement them in our raspberry using the steps below:


Step 1
-----

Save your actual image by creating a backup.

`root@raspberrypi$ cd /boot/`

`root@raspberrypi$ cp kernel.img kernel-backup.img`     # For RPI 0/1

`root@raspberrypi$ cp kernel7.img kernel7-backup.img`  # For RPI 2/3

`root@raspberrypi$ cp kernel7l.img kernel7l-backup.img`     # For RPI 4

Step 2
-----
Replace the old kernel with the new one:



`root@raspberrypi$ cd /`

`root@raspberrypi$ tar xzf /path/to/modules.tgz`

`root@raspberrypi$ rm /path/to/modules.tgz`

`root@raspberrypi$ cd /boot/`

`root@raspberrypi$ tar xzf /path/to/boot.tgz --no-same-owner `

`root@raspberrypi$ rm /path/to/boot.tgz`

Now, reboot and run your freshly installed kernel:

`root@raspberrypi$ reboot`

You can check whether you’re running the new kernel:

`pi@raspberrypi$ uname -a`

Check before the installation with this command to see the version of your kernel.
Once you install and run the command with the new kernel, you are supposed to have 
a new version of this type: ***4.19.118-v7l+***

Pictures can be found in the _img_ folder to see before and after the installation.

